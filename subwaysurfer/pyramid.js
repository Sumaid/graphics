class Pyramid {

	init(gl, x, y, z, x_length, y_length, z_length, url){

        this.programInfo = {};
        this.texture = {};
        this.positionBuffer = {};
        this.textureCoordBuffer = {};    
        this.indexBuffer = {};
        this.position = [x, y, z];
        this.length = [x_length, y_length, z_length]
        this.texture = loadTexture(gl, url);
        this.rotation = 0;
        this.positionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.positionBuffer);
        this.positions = [
          // Base face
          -1.0, -1.0,  0.0,
           1.0, -1.0,  0.0,
           1.0,  1.0,  0.0,
          -1.0,  1.0,  0.0,
      
          // Front face
          -1.0, -1.0,  0.0,
           1.0, -1.0,  0.0,
           0.0,  0.0,  1.0,
      
          // Back face
           1.0,  1.0,  0.0,
           -1.0,  1.0,  0.0,
           0.0,  0.0,  1.0,
      
          // Right face
           1.0,  1.0,  0.0,
           1.0, -1.0,  0.0,
           0.0,  0.0,  1.0,
      
          // Left face
          -1.0,  1.0,  0.0,
          -1.0, -1.0,  0.0,
           0.0,  0.0,  1.0,
        ];
      
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.positions), gl.STATIC_DRAW);
        this.textureCoordBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.textureCoordBuffer);
      
        this.textureCoordinates = [
          // Front
          0.0,  0.0,
          1.0,  0.0,
          1.0,  1.0,
          0.0,  1.0,
          // Back
          0.0,  0.0,
          1.0,  0.0,
          1.0,  1.0,
          // Top
          0.0,  0.0,
          1.0,  0.0,
          1.0,  1.0,
          // Bottom
          0.0,  0.0,
          1.0,  0.0,
          1.0,  1.0,
          // Right
          0.0,  0.0,
          1.0,  0.0,
          1.0,  1.0,
        ];
      
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.textureCoordinates),
                      gl.STATIC_DRAW);
      
        this.indexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
      
        this.indices = [
          0,  1,  2,      0,  2,  3,    
          4,  5,  6,   
          7,  8,  9,  
          10, 11, 12,   
          13, 14, 15,    
        ];
      
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
            new Uint16Array(this.indices), gl.STATIC_DRAW);
    }

	draw(gl, projectionMatrix, viewMatrix, programInfo){

          var myScalingMatrix =  mat4.create();
          myScalingMatrix = new Float32Array([
              this.length[0]/2,   0.0,  0.0,  0.0,
              0.0,  this.length[1]/2,   0.0,  0.0,
              0.0,  0.0,  this.length[2]/2,   0.0,
              0.0,  0.0,  0.0,  1.0  
            ]);
            var myTranslationMatrix =  mat4.create();
            myTranslationMatrix = new Float32Array([
                1.0,   0.0,  0.0,  0.0,
                0.0,  1.0,   0.0,  0.0,
                0.0,  0.0,  1.0,   0.0,
                this.position[0],  this.position[1],  this.position[2],  1.0  
              ]);
          var modelMatrix = mat4.create();
          mat4.multiply(modelMatrix, myScalingMatrix, modelMatrix);
          mat4.multiply(modelMatrix, myTranslationMatrix, modelMatrix);
          mat4.rotate(modelMatrix, modelMatrix, this.rotation, [0,1,0]);     
          
          var modelViewMatrix = mat4.create();
          mat4.multiply(modelViewMatrix, viewMatrix, modelMatrix);
          {  
          const numComponents = 3;
            const type = gl.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            gl.bindBuffer(gl.ARRAY_BUFFER, this.positionBuffer);
            gl.vertexAttribPointer(
                programInfo.attribLocations.vertexPosition,
                numComponents,
                type,
                normalize,
                stride,
                offset);
            gl.enableVertexAttribArray(
                programInfo.attribLocations.vertexPosition);
          }
        
          {
            const numComponents = 2;
            const type = gl.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            gl.bindBuffer(gl.ARRAY_BUFFER, this.textureCoordBuffer);
            gl.vertexAttribPointer(
                programInfo.attribLocations.textureCoord,
                numComponents,
                type,
                normalize,
                stride,
                offset);
            gl.enableVertexAttribArray(
                programInfo.attribLocations.textureCoord);
          }
        
          gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
        
          gl.useProgram(programInfo.program);
        
          gl.uniformMatrix4fv(
              programInfo.uniformLocations.projectionMatrix,
              false,
              projectionMatrix);
          gl.uniformMatrix4fv(
              programInfo.uniformLocations.modelViewMatrix,
              false,
              modelViewMatrix);
        
          gl.activeTexture(gl.TEXTURE0);
        
          gl.bindTexture(gl.TEXTURE_2D, this.texture);
        
          gl.uniform1i(programInfo.uniformLocations.uSampler, 0);
        
          {
            const vertexCount = 18;
            const type = gl.UNSIGNED_SHORT;
            const offset = 0;
            gl.drawElements(gl.TRIANGLES, vertexCount, type, offset);
          }
        
		return gl;
    }

    tick_rotate(deltaTime){
         this.rotation += deltaTime;
    }
    
    tick_move(x, y, z){
      this.position[0] += x;
      this.position[1] += y;
      this.position[2] += z;
    }

}