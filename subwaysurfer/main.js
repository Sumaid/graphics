const COLOR_BACKGROUND = { 'R':132, 'G':203, 'B':233 };
var GAME_SPEED = 1;
var leftgrass = [];
var rightgrass = [];
var lefttrack = [];
var righttrack = [];
var middletrack = [];
var leftwall = [];
var rightwall = [];
var coins = [];
var coins_score = 0;
var attacked = 0;
var player = new Cuboid();
var dog = new Cuboid();
var police = new Cuboid();
var destination = new Cuboid();
var destinationleg_left = new Cuboid();
var destinationleg_right = new Cuboid();
var leftfire = new BootFire();
var greyscale_decider = 0;
var rightfire = new BootFire();
var ironboards = [];
var dangerboxes = [];
var slowboxes = [];
var jboots = [];
var fboots = [];
var canvas = document.querySelector('#glcanvas');
var canvasid = document.getElementById('glcanvas');
var gamefinish = document.getElementById('gamefinish');
var scoreid = document.getElementById('score');
var scorenumberid = document.getElementById('score_number');
var scoretextid = document.getElementById('score_text');
var flashing_counter = 0;
var game_over = 0;
main();

function main() {
  const gl = canvas.getContext('webgl');
  if (!gl) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.');
    return;
  }

  // Vertex shader program
  const vsSource_light = `
  attribute vec4 aVertexPosition;
  attribute vec3 aVertexNormal;
  attribute vec2 aTextureCoord;
  uniform mat4 uNormalMatrix;
  uniform mat4 uModelViewMatrix;
  uniform mat4 uProjectionMatrix;
  varying highp vec3 ambientLight;
  varying highp vec2 vTextureCoord;
  varying highp vec3 vLighting;
  uniform int flashed;
  void main(void) {
    gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
    vTextureCoord = aTextureCoord;
    if (flashed>98)
      ambientLight = vec3(3, 3, 3);
    else
      ambientLight = vec3(1, 1, 1);
    vLighting = ambientLight;
  }
`;

// Fragment shader program

const fsSource_light = `
  precision mediump float;
  varying highp vec2 vTextureCoord;
  varying highp vec3 vLighting;
  highp vec4 texelColor;
  uniform sampler2D uSampler;
  uniform bool greyscale;
  uniform float now;
  void main(void) {
    highp vec4 sample = texture2D(uSampler, vTextureCoord);
    highp float grey = 0.21 * sample.r + 0.71 * sample.g + 0.07 * sample.b;
    if (greyscale==true)
      texelColor = vec4(grey, grey, grey, 1.0);
    else
      texelColor = texture2D(uSampler, vTextureCoord);
    gl_FragColor = vec4(texelColor.rgb *vLighting, texelColor.a);
  }
`;
  const shaderProgram_light = initShaderProgram(gl, vsSource_light, fsSource_light);

  const programInfo_light = {
    program: shaderProgram_light,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgram_light, 'aVertexPosition'),
      vertexNormal: gl.getAttribLocation(shaderProgram_light, 'aVertexNormal'),
      textureCoord: gl.getAttribLocation(shaderProgram_light, 'aTextureCoord'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgram_light, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgram_light, 'uModelViewMatrix'),
      normalMatrix: gl.getUniformLocation(shaderProgram_light, 'uNormalMatrix'),
      uSampler: gl.getUniformLocation(shaderProgram_light, 'uSampler'),
      greyscale: gl.getUniformLocation(shaderProgram_light, 'greyscale'),
      flashed: gl.getUniformLocation(shaderProgram_light, 'flashed'),
    },
  };

  // Vertex shader program
  const vsSource = `
    attribute vec4 aVertexPosition;
    attribute vec2 aTextureCoord;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    varying highp vec2 vTextureCoord;

    void main(void) {
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
      vTextureCoord = aTextureCoord;
    }
  `;

  // Fragment shader program
  const fsSource = `
    varying highp vec2 vTextureCoord;

    uniform sampler2D uSampler;
    uniform bool greyscale;

    void main(void) {
      highp vec4 sample = texture2D(uSampler, vTextureCoord);
      highp float grey = 0.21 * sample.r + 0.71 * sample.g + 0.07 * sample.b;
      if (greyscale==true)
        gl_FragColor = vec4(grey, grey, grey, 1.0);
      else
        gl_FragColor = texture2D(uSampler, vTextureCoord);
    }
  `;

  const shaderProgram = initShaderProgram(gl, vsSource, fsSource);
  const programInfo = {
    program: shaderProgram,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
      textureCoord: gl.getAttribLocation(shaderProgram, 'aTextureCoord'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
      uSampler: gl.getUniformLocation(shaderProgram, 'uSampler'),
      greyscale: gl.getUniformLocation(shaderProgram, 'greyscale'),
    },
  };

  /* Characters Initialization*/
  player.init(gl, 0, 0, 1, 0.5, 0.5, 1.5, 'skin.jpg');
  dog.init(gl, 0, -0.8, 0.5, 0.5, 0.5, 0.5, 'dog.jpg');
  dog.isDog = 1;
  police.init(gl, 0, -1, 0, 0.6, 0.6, 1.6, 'army.jpg');


  /* World Initialization*/
  var depth = 0.0001;
  var trackwidth = 3.2;
  var limit = 10;
  var instances = 12;
  for (var i = 0; i<instances; i++) {
    temp = new Cuboid();
    temp.init(gl, -3, i*limit, 0+depth/2, trackwidth, limit, depth, 'soil.jpg');
    lefttrack.push(temp);
  }
  for (var i = 0; i<instances; i++) {
    temp = new Cuboid();
    temp.init(gl, 0, i*limit, 0+depth/2, trackwidth, limit, depth, 'soil.jpg');
    middletrack.push(temp);
  }
  for (var i = 0; i<instances; i++) {
    temp = new Cuboid();
    temp.init(gl, 3, i*limit, 0+depth/2, trackwidth, limit, depth, 'soil.jpg');
    righttrack.push(temp);
  }
  for (var i = 0; i<instances; i++) {
    temp = new Cuboid();
    temp.init(gl, -1.5, i*limit, 0+depth/2, trackwidth/5, limit, depth, 'grass.jpg');
    leftgrass.push(temp);
  }
  for (var i = 0; i<instances; i++) {
    temp = new Cuboid();
    temp.init(gl, 1.5, i*limit, 0+depth/2, trackwidth/5, limit, depth, 'grass.jpg');
    rightgrass.push(temp);
  }
  for (var i = 0; i<instances; i++) {
    temp = new Wall();
    temp.init(gl, -4, i*limit, 0+2.5, 0.1, limit, 5, 'rsz_wall.jpg');
    leftwall.push(temp);
  }
  for (var i = 0; i<instances; i++) {
    temp = new Wall();
    temp.init(gl, 4, i*limit, 0+2.5, 0.1, limit, 5, 'rsz_wall.jpg');
    rightwall.push(temp);
  }


  /* Objects Initialization*/
  
  //Initialize Coins
  for (i=0; i<3; i++)
  {
    temp = new Coin();
    temp.init(gl, 0, 5+i*2, 1, 0.5, 0.2, 'gold.png');
    coins.push(temp);
  }
  for (i=0; i<0; i++)
  {
    temp = new Coin();
    temp.init(gl, -3, 5+(i+4)*2, 1, 0.5, 0.2, 'gold.png');
    coins.push(temp);
  }
  for (i=0; i<1; i++)
  {
    temp = new Coin();
    temp.init(gl, 0, 5+(i+7)*2, 1, 0.5, 0.2, 'gold.png');
    coins.push(temp);
  }

  //Initialize Iron Board
  var temp;
  var tempboard = new Cuboid();
  tempboard.init(gl, 0, 22, 2.5, 2, 0.1, 2, 'iron.jpg');
  var templeg_left = new Cuboid();
  templeg_left.init(gl, -0.5, 22, 0.5, 0.1, 0.1, 2, 'copper.jpeg');
  var templeg_right = new Cuboid();
  templeg_right.init(gl, 0.5, 22, 0.5, 0.1, 0.1, 2, 'copper.jpeg');
  temp.tempboard = tempboard;
  temp.templeg_left = templeg_left;
  temp.templeg_right = templeg_right;
  ironboards.push(temp);

  for (i=0; i<1; i++)
  {
    temp = new Coin();
    temp.init(gl, 3, 27, 1, 0.5, 0.2, 'gold.png');
    coins.push(temp);
  }

  var temp = new Cuboid();
  temp.init(gl, -3, 30, 0, 1, 0.5, 1, 'gate.jpeg');
  dangerboxes.push(temp);

  for (i=0; i<1; i++)
  {
    temp = new Coin();
    temp.init(gl, 0, 33, 1, 0.5, 0.2, 'gold.png');
    coins.push(temp);
  }

  for (i=0; i<1; i++)
  {
    temp = new Coin();
    temp.init(gl, -3, 34, 1, 0.5, 0.2, 'gold.png');
    coins.push(temp);
  }

  var temp = new BootFire();
  temp.init(gl, -3, 85, 0, 0.6, 1, 'soft.jpg');
  temp.rotation = 90;
  slowboxes.push(temp);
  var temp = new BootFire();
  temp.init(gl, -3, 88, 0, 0.6, 1, 'soft.jpg');
  temp.rotation = 90;
  slowboxes.push(temp);

  var temp = new Boot();
  temp.init(gl, 3, 40, 1, 1, 0.2, 'up.jpg');
  fboots.push(temp);

  var temp;
  var tempboard = new Cuboid();
  tempboard.init(gl, 0, 42, 2.5, 2, 0.1, 2, 'iron.jpg');
  var templeg_left = new Cuboid();
  templeg_left.init(gl, -0.5, 42, 0.5, 0.1, 0.1, 2, 'copper.jpeg');
  var templeg_right = new Cuboid();
  templeg_right.init(gl, 0.5, 42, 0.5, 0.1, 0.1, 2, 'copper.jpeg');
  temp.tempboard = tempboard;
  temp.templeg_left = templeg_left;
  temp.templeg_right = templeg_right;
  ironboards.push(temp);

  for (i=0; i<3; i++)
  {
    temp = new Coin();
    temp.init(gl, -3+i*3, 45, 1, 0.5, 0.2, 'gold.png');
    coins.push(temp);
  }

  var temp = new Boot();
  temp.init(gl, 3, 54, 1, 1, 0.2, 'powerup.png');
  jboots.push(temp);

  var temp = new Cuboid();
  temp.init(gl, -3, 54, 0, 1, 0.5, 1, 'gate.jpeg');
  dangerboxes.push(temp);


  var temp = new Boot();
  temp.init(gl, 3, 20, 1, 1, 0.2, 'up.jpg');
  fboots.push(temp);


  for (i=0; i<4; i++)
  {
    temp = new Coin();
    temp.init(gl, 0, 63+i*5, 1, 0.5, 0.2, 'gold.png');
    coins.push(temp);
  }

  destination.init(gl, 0, 97, 1.5, 2, 0.1, 2, 'finish.jpg');
  destinationleg_left.init(gl, -0.5, 97, 0.5, 0.1, 0.1, 1, 'copper.jpeg');
  destinationleg_right.init(gl, 0.5, 97, 0.5, 0.1, 0.1, 1, 'copper.jpeg');

  /*Frames Per Second*/
  var then = 0;
  function render(now) {
    now *= 0.001;
    const deltaTime = now - then;
    then = now;
    detect_collision();
    tick_elements(deltaTime);
    if ((player.position[1]>97)&&(game_over==0))
    {
        game_over=1;
        gamefinish.innerHTML = "Game Finished!";
        gamefinish.style.color = "green";
        canvasid.style.display = "none";
        gamefinish.style.display = "block";
        scoreid.style.display = "block";
        scorenumberid.innerHTML = coins_score;
    }
    drawScene(gl, programInfo, programInfo_light);
    if (game_over==0)
      requestAnimationFrame(render);
  }
  if (game_over==0)
    requestAnimationFrame(render);
}

function detect_collision(){
  if (player.flyingboots>0)
    return;
  for (i=0; i<coins.length; i++)
  {
    if(position_diff(coins[i], player))
    {
      coins.splice(i, 1);
      coins_score += 1;
    }
  }
  if (position_diff(player, police))
  {
    game_over = 1;
    gamefinish.innerHTML = "Game Over!";
    gamefinish.style.color = "red";
    canvasid.style.display = "none";
    gamefinish.style.display = "block";
    scoreid.style.display = "block";
    scorenumberid.innerHTML = coins_score;
    exit(0);
  }
  for (i=0; i<jboots.length; i++)
  {
    if(position_diff(jboots[i], player))
    {
      jboots.splice(i, 1);
      player.jumpingboots = 500;
    }
  }

  for (i=0; i<fboots.length; i++)
  {
    if(position_diff(fboots[i], player))
    {
      fboots.splice(i, 1);
      player.flyingboots = 900;
    }
  }
  for (i=0; i<slowboxes.length; i++)
  {
    if (slowboxes[i].valid==false)
      continue;
    if(Math.abs(player.position[0]-slowboxes[i].position[0])<0.01&&Math.abs(player.position[1]-slowboxes[i].position[1])<0.01&&Math.abs(player.position[2]-slowboxes[i].position[2])<1.01)
    {
        slowboxes.splice(i,1);
        attacked = 1;
        player_speed *= 1/4;
        continue;
   //     i = 0;
    }
  }
  for (i=0; i<dangerboxes.length; i++)
  {
    if(Math.abs(player.position[0]-dangerboxes[i].position[0])<0.01&&Math.abs(player.position[1]-dangerboxes[i].position[1])<0.01)
    {
      if  (player.jumping==0)
      {
        game_over = 1;
        gamefinish.innerHTML = "Game Over!";
        gamefinish.style.color = "red";
        canvasid.style.display = "none";
        gamefinish.style.display = "block";
        scoreid.style.display = "block";
        scorenumberid.innerHTML = coins_score;
        exit(0);
      }
    }
  }
  for (i=0; i<ironboards.length; i++)
  {
    if(Math.abs(player.position[0]-ironboards[i].tempboard.position[0])<0.01&&Math.abs(player.position[1]-ironboards[i].tempboard.position[1])<0.01)
    {
      if  (player.crawling==0)
      {
        game_over = 1;
        gamefinish.innerHTML = "Game Over!";
        gamefinish.style.color = "red";
        canvasid.style.display = "none";
        gamefinish.style.display = "block";
        scoreid.style.display = "block";
        scorenumberid.innerHTML = coins_score;
        exit(0);
      }
    }
  }
  if (Math.abs(player.position[0]-destination.position[0])<0.01&&Math.abs(player.position[1]-destination.position[1])<0.01)
  {
    game_over = 1;
    gamefinish.innerHTML = "Game Finished!";
    gamefinish.style.color = "green";
    canvasid.style.display = "none";
    gamefinish.style.display = "block";
    scoreid.style.display = "block";
    scorenumberid.innerHTML = coins_score;
    exit(0);
  }
}

function position_diff(a, b){
  if (Math.abs(a.position[0]-b.position[0])<0.01&&Math.abs(a.position[1]-b.position[1])<0.01&&Math.abs(a.position[2]-b.position[2])<1.01)
    return true;
  else 
    return false;
}
var police_speed = 3*GAME_SPEED/100;
var player_speed = GAME_SPEED/10;

function tick_elements(deltaTime) {
  if (player_speed<GAME_SPEED/10)
    player_speed += 0.001;
  else if ((player_speed!=GAME_SPEED/10)&&(attacked==1))
  {
    player_speed = GAME_SPEED/10;
    police_speed = 3*GAME_SPEED/100;
    attacked = 0;
  }
  player.tick_move(0, player_speed, 0);
  dog.tick_move(0, player_speed, 0);
  if (police_speed<GAME_SPEED/10)
    police_speed += 0.001;
  else
    police_speed = GAME_SPEED/10;
  
  police.tick_move(0, police_speed, 0);
}

function drawScene(gl, programInfo, programInfo_light) {
  gl.clearColor(COLOR_BACKGROUND.R/256.0, COLOR_BACKGROUND.G/256.0, COLOR_BACKGROUND.B/256.0, 1.0);  
  gl.clearDepth(1.0);                 
  gl.enable(gl.DEPTH_TEST);           
  gl.depthFunc(gl.LEQUAL);          
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  const fieldOfView = 45 * Math.PI / 180; 
  const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
  const zNear = 0.1;
  const zFar = 200.0;
  var projectionMatrix = mat4.create();

  mat4.perspective(projectionMatrix,
                   fieldOfView,
                   aspect,
                   zNear,
                   zFar);

  var viewMatrix = mat4.create();

  var target = [0, 
                player.position[1],
                1.8
               ];

  var eye = [0, player.position[1]-7, 5];

  var up = [0, 1, 0];

  mat4.lookAt(viewMatrix, eye, target, up);
  
  for (var i = 0; i<leftwall.length; i++) {
    gl = leftwall[i].draw(gl, projectionMatrix, viewMatrix, programInfo_light);
  }
  for (var i = 0; i<rightwall.length; i++) {
    gl = rightwall[i].draw(gl, projectionMatrix, viewMatrix, programInfo_light);
  }
  flashing_counter += 1;
  if (flashing_counter==105)
    flashing_counter = 0;
  gl.uniform1i(programInfo_light.uniformLocations.flashed, flashing_counter);
  gl.uniform1i(programInfo_light.uniformLocations.greyscale, greyscale_decider);

  for (var i = 0; i<lefttrack.length; i++) {
    gl = lefttrack[i].draw(gl, projectionMatrix, viewMatrix, programInfo);
  }
  for (var i = 0; i<righttrack.length; i++) {
    gl = righttrack[i].draw(gl, projectionMatrix, viewMatrix, programInfo);
  }
  for (var i = 0; i<middletrack.length; i++) {
    gl = middletrack[i].draw(gl, projectionMatrix, viewMatrix, programInfo);
  }
  for (var i = 0; i<leftgrass.length; i++) {
    gl = leftgrass[i].draw(gl, projectionMatrix, viewMatrix, programInfo);
  }
  for (var i = 0; i<rightgrass.length; i++) {
    gl = rightgrass[i].draw(gl, projectionMatrix, viewMatrix, programInfo);
  }
  for (var i = 0; i<dangerboxes.length; i++) {
    gl = dangerboxes[i].draw(gl, projectionMatrix, viewMatrix, programInfo);
  }
  for (var i = 0; i<jboots.length; i++) {
    gl = jboots[i].draw(gl, projectionMatrix, viewMatrix, programInfo);
  }
  for (var i = 0; i<fboots.length; i++) {
    gl = fboots[i].draw(gl, projectionMatrix, viewMatrix, programInfo);
  }
  Mousetrap.bind(["left", "a"], () => {
    if (player.position[0] > -3) 
    {
      player.position[0]-=3;
      police.position[0]-=3;      
      dog.position[0]-=3     
    }
  });
  Mousetrap.bind(["d", "right"], () => {
    if (player.position[0] < 3) 
    {
      player.position[0]+=3;
      police.position[0]+=3; 
      dog.position[0]+=3     
    }
  });
  Mousetrap.bind(["space"], () => {
      if (player.jumpingboots < 1)
        player.upspeed = GAME_SPEED/2;
      else
        player.upspeed = 5*GAME_SPEED/8;
      dog.upspeed = GAME_SPEED/2;      
      dog.jumping = 1;      
      player.jumping = 1;
  });
  Mousetrap.bind(["down"], () => {
    player.crawling = 1;
  });
  Mousetrap.bind(["up"], () => {
    player.crawling = 0;
  });
  Mousetrap.bind(["p"], () => {
    if (greyscale_decider==0)
      greyscale_decider = 1;
    else
      greyscale_decider = 0;
  });
  gl = destination.draw(gl, projectionMatrix, viewMatrix, programInfo);
  gl = destinationleg_left.draw(gl, projectionMatrix, viewMatrix, programInfo);
  gl = destinationleg_right.draw(gl, projectionMatrix, viewMatrix, programInfo);
  for (var i = 0; i<slowboxes.length; i++) {
    gl = slowboxes[i].draw(gl, projectionMatrix, viewMatrix, programInfo);
  }
  gl = player.draw(gl, projectionMatrix, viewMatrix, programInfo);

  for (i=0; i<ironboards.length; i++)
  {
    gl = ironboards[i].tempboard.draw(gl, projectionMatrix, viewMatrix, programInfo);
    gl = ironboards[i].templeg_left.draw(gl, projectionMatrix, viewMatrix, programInfo);
    gl = ironboards[i].templeg_right.draw(gl, projectionMatrix, viewMatrix, programInfo);
  }
  for (var i = 0; i<coins.length; i++) {
    gl = coins[i].draw(gl, projectionMatrix, viewMatrix, programInfo);
  }
  
  gl = dog.draw(gl, projectionMatrix, viewMatrix, programInfo);
  gl = police.draw(gl, projectionMatrix, viewMatrix, programInfo);
  gl.uniform1i(programInfo.uniformLocations.greyscale, greyscale_decider);
}

function loadTexture(gl, url) {
  const texture = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, texture);
  const level = 0;
  const internalFormat = gl.RGBA;
  const width = 1;
  const height = 1;
  const border = 0;
  const srcFormat = gl.RGBA;
  const srcType = gl.UNSIGNED_BYTE;
  const pixel = new Uint8Array([COLOR_BACKGROUND.R/256, COLOR_BACKGROUND.G/256, COLOR_BACKGROUND.B/256, 1.0]);  // opaque blue
  gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
                width, height, border, srcFormat, srcType,
                pixel);

  const image = new Image();
  image.crossOrigin = "Anonymous";

  image.onload = function() {
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
                  srcFormat, srcType, image);

    if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
       gl.generateMipmap(gl.TEXTURE_2D);
    } else {
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
       gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    }
  };
  image.src = url;

  return texture;
}

function isPowerOf2(value) {
  return (value & (value - 1)) == 0;
}

function initShaderProgram(gl, vsSource, fsSource) {
  const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vsSource);
  const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource);
  const shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader);
  gl.attachShader(shaderProgram, fragmentShader);
  gl.linkProgram(shaderProgram);
  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
    alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
    return null;
  }
  return shaderProgram;
}

function loadShader(gl, type, source) {
  const shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
    gl.deleteShader(shader);
    return null;
  }
  return shader;
}
