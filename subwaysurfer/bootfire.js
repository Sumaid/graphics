class BootFire {

	init(gl, x, y, z, radius, width, url){

        this.programInfo = {};
        this.texture = {};
        this.positionBuffer = {};
        this.textureCoordBuffer = {};    
        this.indexBuffer = {};
        this.position = [x, y, z];
        this.length = [radius]
        this.texture = loadTexture(gl, url);
        this.rotation = 0;
        this.n = 500;
        this.valid = true;
        this.positionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.positionBuffer);

        this.positions = [];
        for (var i=0;i<this.n;++i){
            this.positions.push(radius*Math.cos(i*2*Math.PI/this.n));
            this.positions.push(0.0);
            this.positions.push(radius*Math.sin(i*2*Math.PI/this.n));

            this.positions.push(radius*Math.cos((i+1)*2*Math.PI/this.n));
            this.positions.push(0.0);
            this.positions.push(radius*Math.sin((i+1)*2*Math.PI/this.n));

            this.positions.push(0.0);
            this.positions.push(width);
            this.positions.push(0.0);
        }
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.positions), gl.STATIC_DRAW);
        this.textureCoordBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.textureCoordBuffer);
      
        this.textureCoordinates = [];
        for (var i=0;i<this.n;++i){
            this.textureCoordinates.push(0.0);
            this.textureCoordinates.push(0.0);

            this.textureCoordinates.push(1.0);
            this.textureCoordinates.push(1.0);
            
            this.textureCoordinates.push(1.0);
            this.textureCoordinates.push(0.0);
        }

        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.textureCoordinates),
                      gl.STATIC_DRAW);
      
        this.indexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
      
        this.indices = []
        for (var i=0; i<this.positions.length/3; i++)
            this.indices.push(i);

        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
            new Uint16Array(this.indices), gl.STATIC_DRAW);
    }

	draw(gl, projectionMatrix, viewMatrix, programInfo){

          var myScalingMatrix =  mat4.create();
          myScalingMatrix = new Float32Array([
              1,   0.0,  0.0,  0.0,
              0.0,  1,   0.0,  0.0,
              0.0,  0.0,  1,   0.0,
              0.0,  0.0,  0.0,  1.0  
            ]);
            var myTranslationMatrix =  mat4.create();
            myTranslationMatrix = new Float32Array([
                1.0,   0.0,  0.0,  0.0,
                0.0,  1.0,   0.0,  0.0,
                0.0,  0.0,  1.0,   0.0,
                this.position[0],  this.position[1],  this.position[2],  1.0  
              ]);
          var modelMatrix = mat4.create();
          mat4.multiply(modelMatrix, myScalingMatrix, modelMatrix);
          mat4.multiply(modelMatrix, myTranslationMatrix, modelMatrix);
          mat4.rotate(modelMatrix, modelMatrix, this.rotation, [1,0,0]);     
          
          var modelViewMatrix = mat4.create();
          mat4.multiply(modelViewMatrix, viewMatrix, modelMatrix);
          {  
          const numComponents = 3;
            const type = gl.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            gl.bindBuffer(gl.ARRAY_BUFFER, this.positionBuffer);
            gl.vertexAttribPointer(
                programInfo.attribLocations.vertexPosition,
                numComponents,
                type,
                normalize,
                stride,
                offset);
            gl.enableVertexAttribArray(
                programInfo.attribLocations.vertexPosition);
          }
        
          {
            const numComponents = 2;
            const type = gl.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            gl.bindBuffer(gl.ARRAY_BUFFER, this.textureCoordBuffer);
            gl.vertexAttribPointer(
                programInfo.attribLocations.textureCoord,
                numComponents,
                type,
                normalize,
                stride,
                offset);
            gl.enableVertexAttribArray(
                programInfo.attribLocations.textureCoord);
          }
        
          gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
        
          gl.useProgram(programInfo.program);
        
          gl.uniformMatrix4fv(
              programInfo.uniformLocations.projectionMatrix,
              false,
              projectionMatrix);
          gl.uniformMatrix4fv(
              programInfo.uniformLocations.modelViewMatrix,
              false,
              modelViewMatrix);
        
          gl.activeTexture(gl.TEXTURE0);
        
          gl.bindTexture(gl.TEXTURE_2D, this.texture);
        
          gl.uniform1i(programInfo.uniformLocations.uSampler, 0);
        
          {
            const vertexCount = this.n*3;
            const type = gl.UNSIGNED_SHORT;
            const offset = 0;
            gl.drawElements(gl.TRIANGLES, vertexCount, type, offset);
          }
        
		return gl;
    }

    tick_rotate(deltaTime){
         this.rotation += deltaTime;
    }
    
    tick_move(x, y, z){
      this.position[0] += x;
      this.position[1] += y;
      this.position[2] += z;
    }

}