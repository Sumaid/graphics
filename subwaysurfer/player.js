class Player {

	init(gl, x, y, z, face_url, body_url, legs_url){
        this.position = [x, y, z];
//        temp = Cuboid();
//
//        this.legs.push(Cuboid(gl, x-0.5, y+0.5, z, 0.1, 0.1, 1, legs_url));
//        this.legs.push(Cuboid(gl, x+0.5, y-0.5, z, 0.1, 0.1, 1, legs_url));
//        this.legs.push(Cuboid(gl, x+0.5, y+0.5, z, 0.1, 0.1, 1, legs_url));
    }

	draw(gl, projectionMatrix, viewMatrix, programInfo){
        gl = this.face.draw(gl, projectionMatrix, viewMatrix, programInfo);
        gl = this.body.draw(gl, projectionMatrix, viewMatrix, programInfo);
        for (i=0; i<this.legs.length; i++)
            gl = this.legs[i].draw(gl, projectionMatrix, viewMatrix, programInfo);
        return gl;
    }

    tick_move(x, y, z){
        this.face.tick_move(x, y, z);
        this.body.tick_move(x, y, z);
        for (i=0; i<4; i++)
            this.legs[i].tick_move(x, y, z);
    }
}